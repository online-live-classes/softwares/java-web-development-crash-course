### Prerequisites

* Knowledge of HTML
* Knowledge of Core Java Programming

### Course Content

#### Day 1

* Recapping HTML and Core Java
* Understanding HTTP and URL
* Web Server and Client
* Understanding Web Container

#### Day 2

* Why we need Servlet and JSPs?
* First Web Application with Servlet and JSP
* Web Application Directory Structure
* Learning Deployment Descriptors

#### Day 3

* Servlet Session Management
* Servlet Filter
* Servlet Listener
* Cookies in Servlet
* Servlet Exception Handling

#### Day 4

* JSP Example
* JSP Implicit Objects
* JSP Directives
* JSP Exception Handling
* JSP Action Tags
* JSP EL Tutorial
* JSTL Tutorial
* JSP Custom Tags

#### Day 5

* Small project implementation